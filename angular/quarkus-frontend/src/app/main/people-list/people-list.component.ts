import { Component, OnInit, OnDestroy } from '@angular/core';
import { Person } from 'app/generals/generals';
import { PeopleService } from 'app/services/people.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-people-list',
  templateUrl: './people-list.component.html',
  styleUrls: ['./people-list.component.css']
})
export class PeopleListComponent implements OnInit, OnDestroy {

  public people: Person[];
  private subscription: Subscription = new Subscription();

  displayedColumns: string[] = ['firstName', 'lastName', 'age'];

  constructor(
    private readonly peopleService: PeopleService
  ) { }

  ngOnDestroy(): void {
    if (!this.subscription.closed) {
      this.subscription.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.subscription.add(this.peopleService.getAllPeople().subscribe(people => {
      this.people = people;
    }));
  }

}
