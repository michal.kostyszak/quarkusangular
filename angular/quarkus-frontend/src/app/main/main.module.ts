import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTableModule } from '@angular/material/table';
import { PeopleListComponent } from './people-list/people-list.component';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MainRoutingModule } from './main-routing.module';
import { PersonDetailsComponent } from './person-details/person-details.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ToastrModule } from 'ngx-toastr';



@NgModule({
  declarations: [
    MainComponent,
    NavbarComponent,
    PeopleListComponent,
    DashboardComponent,
    PersonDetailsComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MainRoutingModule,
    MatInputModule,
    MatToolbarModule,
    MatTableModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    ToastrModule.forRoot()
  ]
})
export class MainModule { }
