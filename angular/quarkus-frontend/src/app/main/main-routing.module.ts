import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { PeopleListComponent } from './people-list/people-list.component';
import { MainComponent } from './main.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PersonDetailsComponent } from './person-details/person-details.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'main',
    pathMatch: 'full'
  },
  {
    path: 'main',
    component: MainComponent,
    children: [
      { path: '', component: DashboardComponent},
      { path: 'list', component: PeopleListComponent},
      { path: 'detail/:id', component: PersonDetailsComponent},
      { path: 'detail', component: PersonDetailsComponent}
    ]
  },
 ];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class MainRoutingModule { }
