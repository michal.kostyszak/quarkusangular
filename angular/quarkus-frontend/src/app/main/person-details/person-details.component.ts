import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { PeopleService } from 'app/services/people.service';
import { Person } from 'app/generals/generals';
import { map } from 'rxjs/internal/operators/map';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-person-details',
  templateUrl: './person-details.component.html',
  styleUrls: ['./person-details.component.css']
})
export class PersonDetailsComponent implements OnInit, OnDestroy {

  personEdit: FormGroup;
  person: Person;
  isIdInURL = false;

  private subscription: Subscription = new Subscription();

  constructor(
    private readonly route: ActivatedRoute,
    private readonly formBuilder: FormBuilder,
    private toastr: ToastrService,
    private readonly peopleService: PeopleService
  ) { }

  ngOnInit(): void {
    this.setForm();
    this.getPerson();
  }

  ngOnDestroy(): void {
    if (!this.subscription.closed) {
      this.subscription.unsubscribe();
    }
  }

  private getPerson(): void {
    let id: number;
    this.route.paramMap.pipe(map(paramMap => paramMap.get('id'))).subscribe(idParam => {
      id = +idParam;
    });
    if (id){
      this.subscription.add(this.peopleService.getOnePersonById(id).subscribe(person => {
        this.person = person;
        this.personEdit.patchValue(this.person);
      }));
    } else {
      this.person = new Person();
    }
  }

  private setForm(): void {
    this.personEdit = this.formBuilder.group({
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      age: [null]
    });
  }

  public save(): void {
    this.getValuesFromForm();
    if (this.person.id !== undefined) {
      this.subscription.add(this.peopleService.updatePerson(this.person, this.person.id).subscribe());
      this.toastr.success('User ' + this.person.firstName + ' ' + this.person.lastName + ' successfully updated');
    } else {
      this.subscription.add(this.peopleService.createPerson(this.person).subscribe());
      this.toastr.success('User ' + this.person.firstName + ' ' + this.person.lastName + ' successfully created');
    }
  }

  getValuesFromForm(): void  {
    this.person.firstName = this.personEdit.get('firstName').value;
    this.person.lastName = this.personEdit.get('lastName').value;
    this.person.age = this.personEdit.get('age').value;
  }

}
