import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import SpyObj = jasmine.SpyObj;
import { PersonDetailsComponent } from './person-details.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PeopleService } from 'app/services/people.service';
import { Person } from 'app/generals/generals';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('PersonDetailsComponent', () => {
  let component: PersonDetailsComponent;
  let fixture: ComponentFixture<PersonDetailsComponent>;
  let peopleService: SpyObj<PeopleService>;
  let testPerson: Person;
  let element: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonDetailsComponent ],
      imports: [
        RouterTestingModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        MatFormFieldModule,
        MatInputModule,
        BrowserAnimationsModule
      ],
      providers: [
        { provide: PeopleService, useValue: peopleService }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    testPerson = {id: 1, firstName: 'Jan', lastName: 'Kowalski', age: 20};
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it('should display person in form', () => {
  //   //given
  //   component.person = testPerson;
  //   element = fixture.nativeElement as HTMLElement;
  //   //when
  //   component.getPerson();
  //   //then
  //   const firstNameElement = element.querySelector<HTMLInputElement>('input#name');
  //   expect(firstNameElement.value).toBe(testPerson.firstName);
  // })

});
