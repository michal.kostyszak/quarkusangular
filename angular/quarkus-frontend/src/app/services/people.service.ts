import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Person } from 'app/generals/generals';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class PeopleService {

  constructor(private http: HttpClient) { }

  getAllPeople(): Observable<Array<Person>> {
    return this.http.get<Array<Person>>(`/people`);
  }

  getOnePersonById(id: number): Observable<Person> {
    return this.http.get<Person>(`/people/${id}`);
  }

  createPerson(person: Person): Observable<Person> {
    return this.http.post<Person>(`/people`, person);
  }

  updatePerson(person: Person, id: number): Observable<Person> {
    return this.http.put<Person>(`/people/${id}`, person);
  }

}
