import { TestBed } from '@angular/core/testing';
import { PeopleService } from './people.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { Person } from 'app/generals/generals';

describe('PeopleService', () => {
  let peopleService: PeopleService;
  let controller: HttpTestingController;
  const person1: Person = {
    id: 1,
    firstName: 'Jan',
    lastName: 'Kowalski',
    age: 23
  };
  const person2: Person = {
    id: 2,
    firstName: 'Adam',
    lastName: 'Kwiatkowski',
    age: 31
  };
  const person3: Person = {
    id: 3,
    firstName: 'Michał',
    lastName: 'Kostyszak',
    age: 32
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [
        PeopleService,
      ]
    });
    peopleService = TestBed.inject(PeopleService);
    controller = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(peopleService).toBeTruthy();
  });

  it('should get all people', done => {
    peopleService.getAllPeople().subscribe(res => {
      expect(res).toEqual([person1, person2, person3]);
      expect(res.length).toEqual(3);
      done();
    }, fail);

    const req = controller.expectOne(`/people`);
    expect(req.request.method).toBe('GET');
    req.flush([person1, person2, person3]);

    controller.verify();
  });

  it('should get one person by his id', done => {
    peopleService.getOnePersonById(1).subscribe(res => {
      expect(res).toEqual(person1);
      done();
    }, fail);

    const req = controller.expectOne(`/people/1`);
    expect(req.request.method).toBe('GET');
    req.flush(person1);

    controller.verify();
  });

  it('should add person', done => {

    peopleService.createPerson(person1).subscribe(res => {
      expect(res).toEqual(person1);
      done();
    }, fail);

    const req = controller.expectOne(`/people`);
    expect(req.request.method).toBe('POST');
    expect(req.request.body).toEqual(person1);
    req.flush(person1);

    controller.verify();
  });

  it('should update person', done => {

    peopleService.updatePerson(person1, person1.id).subscribe(res => {
      expect(res).toEqual(person1);
      done();
    }, fail);

    const req = controller.expectOne(`/people/${person1.id}`);
    expect(req.request.method).toBe('PUT');
    expect(req.request.body).toEqual(person1);
    req.flush(person1);

    controller.verify();
  });

});
