package org.acme.getting.started.repository;

import org.acme.getting.started.entities.PersonEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface PersonRepository extends CrudRepository<PersonEntity, Long> {
}
