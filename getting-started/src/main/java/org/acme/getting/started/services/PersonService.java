package org.acme.getting.started.services;

import org.acme.getting.started.entities.PersonEntity;
import org.acme.getting.started.repository.PersonRepository;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Optional;

@Path("people")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApplicationScoped
public class PersonService {

    @Inject
    PersonRepository personRepository;

    @GET
    public Iterable<PersonEntity> get() {
        return personRepository.findAll();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Optional<PersonEntity> getOne(@PathParam Long id) {
        return personRepository.findById(id);
    }

    @POST
    public PersonEntity create(PersonEntity person) {
        return personRepository.save(person);
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public PersonEntity update(PersonEntity person, @PathParam Long id) {
        PersonEntity personToUpdate = personRepository.findById(id).get();
        personToUpdate = person;
        return personRepository.save(personToUpdate);
    }

}
