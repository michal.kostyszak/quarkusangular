package org.acme.getting.started.services;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.acme.getting.started.entities.PersonEntity;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.MediaType;
import java.awt.*;
import java.util.Optional;

import static io.restassured.RestAssured.given;
import static org.mockito.Mockito.when;

@QuarkusTest
public class PersonServiceTest {

    @InjectMock
    PersonService personService;

    @Test
    public void testGetAllEndpoint() {
        given()
                .when().get("/people")
                .then()
                .statusCode(200);
    }

    @Test
    public void testGetOneEndpoint() {
        Long id = 1L;
        when(personService.getOne(id)).thenReturn(Optional.of(new PersonEntity(1L, "Adam", "Kowalski", 23)));
        given()
                .pathParam("id", id)
                .when().get("/people/{id}")
                .then()
                .statusCode(200);
    }

    @Test
    public void testPostEndpoint() {
        PersonEntity personEntity = new PersonEntity("Jacek", "Rolski", 50);
        when(personService.create(personEntity)).thenReturn(new PersonEntity(1L, "Jacek", "Rolski", 50));
        given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(personEntity)
                .when().post("/people")
                .then()
                .statusCode(204);
    }

    @Test
    public void testPutEndpoint() {
        Long id = 1L;
        PersonEntity updatedPersonEntity = new PersonEntity("Tomasz", "Kowalski", 23);
        when(personService.update(updatedPersonEntity, id)).thenReturn(new PersonEntity(1L, "Tomasz", "Kowalski", 23));
        given()
                .pathParam("id", id)
                .contentType(MediaType.APPLICATION_JSON)
                .body(updatedPersonEntity)
                .when().put("/people/{id}")
                .then()
                .statusCode(204);
    }

}
